import Gun from 'gun/gun';
import type { IGunChainReference } from 'gun/types/chain';

declare module "gun/types/chain" {
  interface IGunChainReference<
    DataType = Record<string, any>,
    ReferenceKey = any,
    IsTop extends "pre_root" | "root" | false = false
  > {
    subscribe(subscription: (value: DataType) => void): () => void;
  }
}

declare global {
  interface StringConstructor {
    random(): string;
  }
  interface Window {
    db: IGunChainReference;
  }
}

const db = new Gun("https://gun-us.herokuapp.com/gun");
window.db = db;

Gun.chain.subscribe = function <DataType>(
  this: IGunChainReference,
  subscription: (value: DataType) => void
) {
  const connection = this.on(subscription);
  return () => connection.off();
};

export default db;
